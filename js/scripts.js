$(document).ready(function () {
  // Begin League
  // $( ".nextbutton--start" ).click(function() {
  //   $(".fixtures").show("slide", { direction: "right" }, 1000);
  //   $(".team-maker").hide("slide", { direction: "left" }, 1000);
  //   console.log("next panel");
  // });

  var teamNum = 0;

  var team = [];
  var teamID = [];
  var colors = [];
  var teamName;

  function setTeam() {
    // console.log(teamNum);

    // teamName = $(this).parent().parent().find('input').val();
    // console.log("team name: " + teamName);
    // console.log("colour: " + teamCol);

    var i = teamNum;
    team[i] = teamName;
    colors[i] = teamCol;
    teamID[i] = "team--" + teamNum;
    teamNum++;

    // Add to leader board
    $(".points").children(".team-position:first").clone().appendTo(".points").removeClass('hidden').addClass(teamID[i]).addClass(colors[i]).data('id',teamID[i]).attr('data-id',teamID[i]).children('.team-name').html(teamName); // .parent().children('.team-pos').html(teamNum)
  }
 var x = 1;

  function createMatches() {
    for (var t = 1; t < team.length; t++) {
      for (var i = 0; i < team.length -1; i++) {
        if (t != i) {
          // console.log(team[i] + " vs. " + team[t]);
          $(".qual-rounds").children(".round:first").clone().show().appendTo(".qual-rounds").children('.team-a ,.score-1').addClass(teamID[i]).html(team[i]).data('id',teamID[i]).attr('data-id',teamID[i]).parent().children('.team-b ,.score-2').addClass(teamID[x]).html(team[x]).data('id',teamID[x]).attr('data-id',teamID[x]).show();
        }
      }
      x++;
    }
  }


  //Randomizer
  $.fn.randomize = function(selector){
    (selector ? this.find(selector) : this).parent().each(function(){
        $(this).children(selector).sort(function(){
            return Math.random() - 0.5;
        }).detach().appendTo(this);
    });

    console.log("Randomize matches");
    return this;
};

  // Start League
 

  $(document).on('click', ".nextbutton--start", function startLeague() {
    
    if($('.team-card--inner input:text').val() && $('.team-card--inner input:checked').val()) {

      $('.teamname-validation').hide()

      // confirmation msg
    $(".team-maker .team-card--inner").slideUp();
    setTimeout(function () {
      $(".icon__success").addClass(teamCol);
      $(".team-maker-success--team-name").html(teamName);
      $(".team-maker").first().find(".team-maker-success").slideDown();
    }, 400);

    $(this).parents(".team-maker").next().find('.team-card--inner').slideDown();
    $(".fixtures").delay(2000).show("slide", { direction: "right" }, 1500);

    //Get form data here ===== This stuff needs copying into both fucntions
    teamName = $(this).parent().parent().find('.teamname').val();
    teamCol = $(this).parent().parent().find("input:radio[name ='color']:checked").attr('id');
    //=======================


    // Disable used colors
    $('.radio-button').find("input:radio[name ='color']:checked").attr('disabled', true);

    $('input[type="radio"]').prop('checked', false); 

    setTeam();
    $(this).parents(".team-maker").delay(2500).hide("slide", { direction: "left" }, 500);
    setTimeout(function () {
      $(".team-maker").first().remove();

      $(".team-maker .team-card--inner").show();
      $(".team-maker").find(".team-maker-success").hide();
      $(".icon__success").removeClass(teamCol);
      // console.log("delete old team " + $(this).parents());
    }, 3500);
  } else {
    $('.teamname-validation').show();
  }
   
    createMatches();
    $('.qual-rounds li').randomize();
  });

  // Create new team
  $(document).on('click', ".nextbutton--newteam", function addNewTeam() {
    // console.log("Add team");

    if($('.team-card--inner input:text').val() && $('.team-card--inner input:checked').val()) {
      $('.teamname-validation').hide()

    $(this).parents(".team-maker").clone().appendTo(".team-maker-wrapper").hide();


    // confirmation msg
    $(".team-maker .team-card--inner").slideUp();
    setTimeout(function () {
      $(".icon__success").addClass(teamCol);
      $(".team-maker-success--team-name").html(teamName);
      $(".team-maker").first().find(".team-maker-success").slideDown();
    }, 400);

    $(this).parents(".team-maker").next().find('.team-card--inner').slideDown();
    $(this).parents(".team-maker").next().delay(2000).show("slide", { direction: "right" }, 1500);
    //set max teams
    console.log(teamNum);
    if (teamNum == 3) {
      $('.nextbutton--newteam').addClass('disabled').prop('disabled', true)
    }


    //Get form data here ===== This stuff needs copying into both fucntions
    teamName = $(this).parent().parent().find('.teamname').val();
    teamCol = $(this).parent().parent().find("input:radio[name ='color']:checked").attr('id');
    //=======================


    // Disable used colors
    $('.radio-button').find("input:radio[name ='color']:checked").attr('disabled', true);

    // reset radios
    $('input[type="radio"]').prop('checked', false); 

    // Reset input
    $('.teamname[type="text"]').val("");


    setTeam();
    $(this).parents(".team-maker").delay(2500).hide("slide", { direction: "left" }, 500);
    setTimeout(function () {
      $(".team-maker").first().remove();

      $(".team-maker .team-card--inner").show();
      $(".team-maker").find(".team-maker-success").hide();
      $(".icon__success").removeClass(teamCol);
      // console.log("delete old team " + $(this).parents());
    }, 3500);
  } else {
    $('.teamname-validation').show();
  }

  });

  function sortTeams() {
    $(".points li").sort(sort_li).appendTo('.points');
  function sort_li(a, b) {
    return ($(b).data('score')) > ($(a).data('score')) ? 1 : -1;
  }
    var teamPos = $('.team-position:visible').find('.team-name').toArray().map(function(i){ return i.innerText });
    // console.log(teamPos);

    // This should all be converted to a loop when I'm being less lazy...
    
    teamPos1 = teamPos[0];
    teamPos2 = teamPos[1];
    teamPos3 = teamPos[2];
    teamPos4 = teamPos[3];

    $('#semi-1a').html(teamPos1);
    $('#semi-1b').html(teamPos3);
    $('#semi-2a').html(teamPos2);
    $('#semi-2b').html(teamPos4);

    
  }

  // Scoring
  $(document).click(function() {


  $(".score").change(function () {

    //Get team points
    var sum = 0;
    var teamID = $(this).data('id');

    // console.log("this " + $(this).text());
    $('.' + teamID + ':input').each(function(){
      // console.log("val " + $(this).val());
      // console.log(this.value);
    sum += parseFloat(this.value);
    });
    $('.team-position.' + teamID).attr('data-score', sum).data('score',sum).children('.team-pts').html(sum);
    // console.log("sum " + sum);
    sortTeams();
    // console.log( $('.team-position.' + teamName).parent());
    // .hide("slide", { direction: "right" }, 1000).delay(1000).show("slide", { direction: "right" }, 1000)

    var scoreA = parseInt($(this).parents().children(".score-1").val());
    var scoreB = parseInt($(this).parents().children(".score-2").val());
    console.log(scoreA + "vs" + scoreB);
    if (scoreA > scoreB) {
      $(this).parent().children(".team-name").removeClass("draw");
      $(this).parent().children().first(".team-name").addClass("winner").removeClass("loser");
      $(this).parent().children().last(".team-name").addClass("loser").removeClass("winner");
    } if (scoreA < scoreB) {
      $(this).parent().children(".team-name").removeClass("draw");
      $(this).parent().children().last(".team-name").addClass("winner").removeClass("loser");
      $(this).parent().children().first(".team-name").addClass("loser").removeClass("winner");
    } if (scoreA == scoreB) {
      $(this).parent().children(".team-name").removeClass("loser winner").addClass("draw");
    }

  });
});

// GoTo Semi-final

$(document).on('click', ".nextbutton--semifinals", function startLeague() {
  $(".semi-finals").show("slide", { direction: "right" }, 1000);
  $(".fixtures").hide("slide", { direction: "left" }, 1000);
});

$(document).on('click', ".nextbutton--final", function startLeague() {
  $(".finals").show("slide", { direction: "right" }, 1000);
  $(".semi-finals").hide("slide", { direction: "left" }, 1000);
  var teamScore1 = parseInt($('#semi-1a').siblings('.score-1').val());
    var teamScore2 = parseInt($('#semi-1b').siblings('.score-2').val());
    var teamScore3 = parseInt($('#semi-2a').siblings('.score-1').val());
    var teamScore4 = parseInt($('#semi-2b').siblings('.score-2').val());
    console.log(teamScore1 + typeof(teamScore1));
    console.log(teamScore2 + typeof(teamScore2));
    console.log(teamScore3 + typeof(teamScore3));
    console.log(teamScore4 + typeof(teamScore4));


    
    console.log("if " + teamScore1 + " > " + teamScore2 + " then score 1 should be first");
    if (teamScore1 > teamScore2) {
      $('#final-a').html($('#semi-1a').html());
    } else {
      $('#final-a').html($('#semi-1b').html());
    } if (teamScore3 > teamScore4) {
      $('#final-b').html($('#semi-2a').html());
    } else {
      $('#final-b').html($('#semi-2b').html());
    }
    // if (teamScore1 > teamScore2) {
    //   $('#final-a').html(teamPos1);
    // } else if (teamScore1 < teamScore2) {
    //   $('#final-a').html(teamPos3);
    // } else {
    //   $('#final-a').html("error");
    // }

    // if (teamScore3 > teamScore4) {
    //   $('#final-b').html(teamPos2);
    // } else if (teamScore3 < teamScore4) {
    //   $('#final-b').html(teamPos4);
    // } else {
    //   $('#final-b').html("error");
    // }

});


});